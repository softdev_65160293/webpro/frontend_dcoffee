type User = {
  id?: number
  email: string
  username: string
  password: string
  fullName: string
  gender: string
  role: string
  image: string
}

export type { User }
