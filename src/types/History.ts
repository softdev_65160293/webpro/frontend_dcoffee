import type { User } from './User'

type History = {
  id: number
  timeDate: Date
  user?: User
  late: boolean
}

export { type History }
