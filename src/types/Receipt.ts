import type { User } from './User'
import type { Member } from './Member'
import type { ReceiptItem } from './ReceiptItem'

type Receipt = {
  id: number
  createdDate: Date
  totalBefore: number
  total: number
  receicedAmount: number
  change: number
  memberDiscount: number
  paymentType: string
  userId?: number
  memberID?: number
  user?: User
  member?: Member
  recieptItem?: ReceiptItem[]
}

export type { Receipt }
