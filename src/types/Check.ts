import type { User } from './User'
import type { Material } from './Material'
import type { CheckItems } from './CheckItems'

type Check = {
  id: number
  created: Date
  totalQty: number
  totalPrice: number
  userId: number
  user?: User
  checkItems?: CheckItems[]
}

export type { Check }
