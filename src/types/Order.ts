import type { OrderItems } from './OrderItems'
import type { User } from './User'

type OrderReceipt = {
  id: number
  created: Date
  totalQty: number
  totalPrice: number
  userId: number
  user?: User
  orderItems?: OrderItems[]
}

export type { OrderReceipt }
