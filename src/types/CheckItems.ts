import type { Material } from './Material'

type CheckItems = {
  id: number
  name: string
  price: number
  unit: number
  materialId: number
  material?: Material
}

export { type CheckItems }
