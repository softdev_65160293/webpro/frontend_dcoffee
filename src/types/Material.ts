import type { MaterialType } from './MaterialType'

type Material = {
  id?: number
  name: string
  price: number
  unit: number
  minimum: number
  type: MaterialType
}

export type { Material }
