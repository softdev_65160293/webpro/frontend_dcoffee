type MaterialType = {
  id?: number
  name: string
}

export type { MaterialType }
