import type { ProductType } from './ProductType'

type Product = {
  id?: number
  name: string
  price: number
  type: ProductType
  image: string
}
function getImageUrl(product: Product) {
  return `http://localhost:3000/images/products/${product.image}`
}
export { type Product, getImageUrl }
