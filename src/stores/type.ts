import { ref } from 'vue'
import { defineStore } from 'pinia'
import typeService from '@/services/type'
import type { MaterialType } from '@/types/MaterialType'

export const useTypeStore = defineStore('type', () => {
  const types = ref<MaterialType[]>([])
  const initialType: MaterialType = {
    name: ''
  }
  const editedType = ref<MaterialType>(JSON.parse(JSON.stringify(initialType)))

  async function getType(id: number) {
    const res = await typeService.getType(id)
    editedType.value = res.data
  }
  async function getTypes() {
    const res = await typeService.getTypes()
    types.value = res.data
    console.log(types.value)
  }

  async function saveType() {
    const type = editedType.value
    if (!type.id) {
      // Add new
      console.log('Post ' + JSON.stringify(type))
      const res = await typeService.addType(type)
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(type))
      const res = await typeService.updateType(type)
    }
    await getTypes()
  }
  async function deleteType() {
    const type = editedType.value
    const res = await typeService.delType(type)

    await getTypes()
  }

  function clearForm() {
    editedType.value = JSON.parse(JSON.stringify(initialType))
  }
  return { types, getTypes, saveType, deleteType, editedType, getType, clearForm }
})
