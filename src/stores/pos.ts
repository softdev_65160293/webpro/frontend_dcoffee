import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useReceipetStore } from './receipt'
import type { Product } from '@/types/Product'
import productService from '@/services/product'
import { useMembersStore } from './member'

export const usePosStore = defineStore('pos', () => {
    const memberStore = useMembersStore()
    const receiptStore = useReceipetStore()
    const dialog = ref(false)
    const dialogDelete = ref(false)
    const dialogNoitem = ref(false)
    const dialogCashNotEnough = ref(false)
    
    //Hot Drinks
    const products1 = ref<Product[]>([])
    //Cold Drinks
    const products2 = ref<Product[]>([])
    //Frappé Drinks
    const products3 = ref<Product[]>([])
    //Dessert
    const products4 = ref<Product[]>([])

    async function getProducts() {
        try {
           let res = await productService.getProductsByType(1)
           products1.value = res.data
           res = await productService.getProductsByType(2)
           products2.value = res.data
           res = await productService.getProductsByType(3)
           products3.value = res.data
           res = await productService.getProductsByType(3)
           products4.value = res.data
        } catch (e: any) {
          console.log(e)
        }
      }

    const showReceipt = function () {
        if (receiptStore.receipt!.total === 0 && receiptStore.receipt!.paymentType === 'scan') {
          dialogNoitem.value = true
        } else if (
          receiptStore.receipt!.receicedAmount === 0 &&
          receiptStore.receipt!.paymentType === 'scan'
        ) {
          receiptStore.showReceiptDialog()
        } else if (receiptStore.receipt!.total === 0) {
          dialogNoitem.value = true
        } else if (receiptStore.receipt!.receicedAmount < receiptStore.receipt!.total) {
          dialogCashNotEnough.value = true
        } else {
          receiptStore.showReceiptDialog()
        }
      }
      
      function closeDialog() {
        dialogNoitem.value = false
        dialogCashNotEnough.value = false
      }
      
      const showDelete = function () {
        if (
          receiptStore.receipt!.totalBefore > 0 ||
          memberStore.editedMember ||
          receiptStore.receipt!.receicedAmount > 0
        ) {
          dialogDelete.value = true
        } else {
          return
        }
      }
      
      const deleteConfirmed = () => {
        receiptStore.clear()
        dialogDelete.value = false
      }
      
      const deleteCancelled = () => {
        dialogDelete.value = false
      }

  return { deleteCancelled, deleteConfirmed, showDelete, closeDialog, showReceipt, getProducts, 
    dialogDelete, dialogNoitem, dialogCashNotEnough, dialog, products1, products2, products3, products4
    }
})
