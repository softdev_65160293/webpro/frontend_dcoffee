import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
import materialService from '@/services/material'
import checkService from '@/services/checkMat'
import type { Material } from '@/types/Material'
import { useMessageStore } from './message'
import type { Check } from '@/types/Check'
import type { CheckItems } from '@/types/CheckItems'
import { useAuthStore } from './auth'

export const useStockStore = defineStore('stock', () => {
  const messageStore = useMessageStore()
  const authStore = useAuthStore()
  const currentUser = authStore.getCurrentUser()
  const initilMaterial: Material = {
    name: '',
    price: 0,
    unit: 0,
    minimum: 0,
    type: { id: 1, name: 'pack' }
  }

  const editedMaterial = ref<Material>(JSON.parse(JSON.stringify(initilMaterial)))
  const materials = ref<Material[]>([])
  const initialCheck = ref<Material[]>([])
  const lowMaterial = ref<Material[]>([])
  const checkList = ref<Check[]>([])

  const checkItems = ref<CheckItems[]>([])
  const check = ref<Check>()

  const CheckDialog = ref(false)
  const form = ref(false)
  const loading = ref(false)
  const dialogConfirm = ref(false)
  const dialogReject = ref(false)
  const dialogLowStock = ref(false)
  initCheck()

  function initCheck(){
    check.value = {
      id: 0,
      created: new Date(),
      totalQty: 0,
      totalPrice: 0,
      userId: currentUser!.id!,
      user: currentUser!, 
    }
    checkItems.value = []
  }

  watch(checkItems, () => {
    calCheck()
  },{ deep: true })

  function CheckStock() {
    CheckDialog.value = true
  }

  const onSubmit = function () {}

  function closeDialog() {
    initCheck()
    CheckDialog.value = false
  }

  function saveUnit() {
    dialogConfirm.value = true
  }

  async function getChecks(){
    const res = await checkService.getChecks()
    checkList.value = res.data
  }

  function closeDialogCon() {
    dialogConfirm.value = false
  }

  async function Confirm() {
    try {
        await checkService.addCheck(check.value!, checkItems.value!)
        console.log(check)
    } catch (error) {
        dialogReject.value = true
    }
    closeDialogCon()
    closeDialog()
  }

  function checkLowStock(){
    for (let i = 0; i < materials.value.length; i++) {
      if( materials.value[i].unit < materials.value[i].minimum && !isMaterialInLowList(materials.value[i])){
        lowMaterial.value.push(materials.value[i])
      }
    }
  }

  function isMaterialInLowList(material: Material) {
    return lowMaterial.value.some(item => item.id === material.id);
  }

  function lowStockDialog(){
    dialogLowStock.value = true
  }

  const calCheck = function () {
    check.value!.totalPrice = 0
    check.value!.totalQty = 0
    for(let i=0; i<checkItems.value.length; i++){
      check.value!.totalPrice += checkItems.value[i].price * checkItems.value[i].unit
      check.value!.totalQty += checkItems.value[i].unit
    }
  }

  function close() {
    dialogReject.value = false
  }

  function closeLowMat(){
    dialogLowStock.value = false
  }

  function clearForm() {
    editedMaterial.value = JSON.parse(JSON.stringify(initilMaterial))
  }

  async function getMaterial(id: number) {
    const res = await materialService.getMaterial(id)
    editedMaterial.value = res.data
  }

  async function getMaterials() {
    const res = await materialService.getMaterials()
    materials.value = res.data
    initialCheck.value = res.data
  }

  async function saveMaterial() {
    const material = editedMaterial.value
    try {
      if (!material.id) {
        const res = await materialService.addMaterial(material)
        console.log(res.data)
      } else {
        const res = await materialService.updateMaterial(material)
      }
    } catch (e: any) {
      messageStore.showMessage(e.message)
    }
    await getMaterials()
  }

  async function deleteMaterial() {
    const material = editedMaterial.value
    const res = await materialService.removeMaterial(material)
    await getMaterials()
  }

  function addUnit(items: Material[]) {
    for (let i = 0; i < items.length; i++) {
        if (items[i].unit > -1) {
            // Update material
            materialService.updateMaterial(items[i]);
            // Update check item list
            const index = initialCheck.value.findIndex(material => material.id === items[i].id);
            initialCheck.value[index].unit = items[i].unit;
            const newCheckItem = {
                id: -1,
                name: items[i].name,
                price: items[i].price,
                unit: items[i].unit,
                materialId: items[i].id!,
            };
            checkItems.value.push(newCheckItem);
        } else {
            dialogReject.value = true;
        }
    }
    checkLowStock()
    Confirm();
  }

  return {
    CheckStock,
    onSubmit,
    closeDialog,
    deleteMaterial,
    saveMaterial,
    getMaterial,
    getMaterials,
    clearForm,
    saveUnit,
    closeDialogCon,
    close,
    Confirm,
    addUnit,
    getChecks,
    checkLowStock,
    closeLowMat,
    lowStockDialog,
    lowMaterial,
    dialogLowStock,
    initialCheck,
    checkList,
    dialogReject,
    CheckDialog,
    form,
    loading,
    editedMaterial,
    materials,
    dialogConfirm,
  }
})
