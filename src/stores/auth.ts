import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'
import { fa } from 'vuetify/locale'
import { useRouter } from 'vue-router'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'
import authService from '@/services/auth'

export const useAuthStore = defineStore('auth', () => {
  const currentUser = ref<User | null>({
    id: 1,
    email: 'Dcoffee@gmail.com',
    username: 'admin',
    password: '1234',
    fullName: 'Admin Staff',
    gender: 'male',
    role: 'user',
    image: ''
  })

  const defaultUser = ref<User | null>({
    id: 1,
    email: 'Dcoffee@gmail.com',
    username: 'admin',
    password: '1234',
    fullName: 'Admin Staff',
    gender: 'male',
    role: 'user',
    image: ''
  })

  const nonUser = ref<User | null>({
    id: 0,
    email: '',
    username: '',
    password: '',
    fullName: '',
    gender: 'male',
    role: 'user',
    image: ''
  })

  const users = ref<User[]>([
    {
      id: 1,
      email: 'Dcoffee@gmail.com',
      username: 'admin1',
      password: '1234',
      fullName: 'Admin1 Staff1',
      gender: 'male',
      role: 'user',
      image: ''
    },
    {
      id: 2,
      email: 'Dcoffee@gmail.com',
      username: 'admin2',
      password: '1234',
      fullName: 'Admin2 Staff2',
      gender: 'male',
      role: 'user',
      image: ''
    },
    {
      id: 3,
      email: 'Dcoffee@gmail.com',
      username: 'admin3',
      password: '2345',
      fullName: 'Admin3 Staff3',
      gender: 'male',
      role: 'user',
      image: ''
    }
  ])

  const status = ref(false)

  // const searchUser = (username: string, password: string) => {
  //   const indexUsername = users.value.findIndex((item) => item.username === username)
  //   const indexPassword = users.value.findIndex((item) => item.password === password)
  //   if (indexUsername < 0 && indexPassword < 0) {
  //     currentUser.value = null
  //     status.value = false
  //   } else if (password === users.value[indexUsername].password) {
  //     currentUser.value = users.value[indexUsername]
  //     status.value = true
  //   }
  // }

  const messageStore = useMessageStore()
  const router = useRouter()
  const loadingStore = useLoadingStore()

  const login = async function (username: string, password: string) {
    loadingStore.doLoad()
    try {
      const resUser = await authService.login(username, password)
      localStorage.setItem('user', JSON.stringify(resUser.data.user))
      localStorage.setItem('access_token', resUser.data.access_token)
      const strUser = localStorage.getItem('user')
      const nowTel = ref('')
      if (strUser) {
        const parsedUser = JSON.parse(strUser)
        nowTel.value = parsedUser.tel
      }
      status.value = true
      messageStore.closeErrorLogin()
      if (nowTel.value != null) {
        router.push('/MainMenuMember')
      } else if (nowTel.value == null) {
        router.push('/MainMenu')
      }
    } catch (e: any) {
      console.log(e.message)
      messageStore.errorDialog = true
    }
    loadingStore.finish()
  }

  const logout = function () {
    router.push('/login')
    localStorage.removeItem('user')
    localStorage.removeItem('access_token')
    currentUser.value = defaultUser.value
  }

  function getCurrentUser(): User | null {
    const strUser = localStorage.getItem('user')
    if (strUser === null) return null
    return JSON.parse(strUser)
  }

  function getToken(): String | null {
    const strToken = localStorage.getItem('access_token')
    if (strToken === null) return null
    return strToken
  }

  let onHome = true
  let onMain = false

  function loginSucess() {
    onHome = false
    onMain = true
  }

  const openNvaAdmin = ref(true)
  const openNvaStaff = ref(true)
  const appBarLogin = ref(true)
  const appBarUnlogin = ref(true)

  async function checkRole() {
    const nowUser = await localStorage.getItem('user')
    if (nowUser === null) return null
    const parsedUser = await JSON.parse(nowUser)
    const userRole = await parsedUser.role

    const roleStatus = ref(0)
    console.log(userRole)
    if (userRole === 'Admin') {
      roleStatus.value = 1
    } else if (userRole === 'Owner') {
      roleStatus.value = 2
    } else if (userRole === 'User') {
      roleStatus.value = 0
    }

    // if (roleStatus.value === 1) {
    //   console.log(roleStatus.value)
    //   openNvaAdmin.value = true
    //   openNvaStaff.value = false
    // } else if (roleStatus.value === 0) {
    //   console.log(roleStatus.value)
    //   openNvaAdmin.value = false
    //   openNvaStaff.value = true
    // }
  }

  // function changeAppBar() {
  //   appBarLogin.value = true
  //   appBarUnlogin.value = false
  // }

  return {
    currentUser,
    users,
    status,
    onHome,
    onMain,
    openNvaStaff,
    openNvaAdmin,
    // changeAppBar,
    loginSucess,
    // searchUser,
    checkRole,
    appBarLogin,
    appBarUnlogin,
    defaultUser,
    nonUser,
    login,
    logout,
    getCurrentUser,
    getToken
  }
})
