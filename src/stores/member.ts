import { nextTick, ref } from 'vue'
import { defineStore } from 'pinia'
import memberService from '@/services/member'
import type { Member } from '@/types/Member'
import { useMessageStore } from './message'
import { useLoadingStore } from './loading'
import { useAuthStore } from './auth'
import { tr } from 'vuetify/locale'

export const useMembersStore = defineStore('member', () => {
  const messageStore = useMessageStore()
  const authStore = useAuthStore()
  const loadingStore = useLoadingStore()
  const initilMember: Member & { files: File[] } = {
    email: '',
    name: '',
    username: '',
    password: '',
    tel: '',
    point: 0,
    gender: '',
    image: 'noimage.jpg',
    files: []
  }

  const editedMember = ref<Member & { files: File[] }>(JSON.parse(JSON.stringify(initilMember)))
  const members = ref<Member[]>([])

  const CheckDialog = ref(false)
  const form = ref(false)
  const loading = ref(false)
  const notFoundAlert = ref(false)

  const onSubmit = function () {}

  // function closeDialog() {
  //   CheckDialog.value = false
  //   nextTick(() => {
  //     editedMember.value = Object.assign({}, initilMember)
  //   })
  // }

  async function getMember(id: number) {
    loadingStore.doLoad()
    const res = await memberService.getMember(id)
    editedMember.value = res.data
    loadingStore.finish()
  }

  async function getMemberByTel(tel: string) {
    try {
      const res = await memberService.getMemberByTel(tel);
      if (res.data) {
        editedMember.value = res.data;
      }
    } catch (error) {
      notFoundAlert.value = true;
    }
    return editedMember.value;
  }

  async function getMembers() {
    try {
      loadingStore.doLoad()
      const res = await memberService.getMembers()
      members.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }

  async function saveMember() {
    loadingStore.doLoad()
    const member = editedMember.value
    if (!member.id) {
      // Add new
      const res = await memberService.addMember(member)
    } else {
      // Update
      const res = await memberService.updateMember(member)
    }

    await getMembers()
    loadingStore.finish()
  }

  async function registerMember() {
    loadingStore.doLoad()
    const member = editedMember.value
    if (!member.id) {
      // Add new
      const res = await memberService.addMember(member)
      authStore.login(member.username, member.password)
    } else {
      messageStore.errorDialog = true
    }

    await getMembers()
    loadingStore.finish()
  }

  async function deleteMember() {
    loadingStore.doLoad()
    const member = editedMember.value
    const res = await memberService.removeMember(member)

    await getMembers()
    loadingStore.finish()
  }

  function clearForm() {
    editedMember.value = JSON.parse(JSON.stringify(initilMember))
    getMembers()
  }

  function closeNotFound(){
    notFoundAlert.value = false
  }

  return {
    onSubmit,
    // closeDialog,
    deleteMember,
    saveMember,
    getMember,
    registerMember,
    getMembers,
    clearForm,
    getMemberByTel,
    closeNotFound,
    notFoundAlert,
    CheckDialog,
    form,
    loading,
    editedMember,
    members
  }
})
