import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { Salary } from '@/types/Salary'
import { useHistoryStore } from './history'
import { useAuthStore } from './auth'

export const useSalarysStore = defineStore('salarys', () => {
  const defaultsalary: Salary = {
    id: 0,
    name: '',
    email: '',
    salary: '',
    status: false,
    date: new Date()
  }
  const Paystatus = ref<boolean>(false)
  const historyStore = useHistoryStore()
  const authStore = useAuthStore()

  const salarys = ref<Salary[]>([
    {
      id: 1,
      name: authStore.users[0].fullname,
      email: authStore.users[0].email,
      salary: '15000',
      status: false,
      date: new Date(2024, 1, 2, 11, 20, 30),
      userId: authStore.users[0].id,
      user: authStore.users[0],
      historyCheckin: historyStore.historyCheckIn.filter(
        (checkIn) => checkIn.user?.id === authStore.users[0].id
      ),
      historyCheckout: historyStore.historyCheckOut.filter(
        (checkOut) => checkOut.user?.id === authStore.users[0].id
      )
    },
    {
      id: 2,
      name: authStore.users[1].fullname,
      email: authStore.users[1].email,
      salary: '20000',
      status: false,
      date: new Date(2024, 1, 1, 9, 45, 40),
      userId: authStore.users[1].id,
      historyCheckin: historyStore.historyCheckIn.filter(
        (checkIn) => checkIn.user?.id === authStore.users[1].id
      ),
      historyCheckout: historyStore.historyCheckOut.filter(
        (checkOut) => checkOut.user?.id === authStore.users[1].id
      )
    },
    {
      id: 3,
      name: authStore.users[2].fullname,
      email: authStore.users[2].email,
      salary: '15000',
      status: false,
      date: new Date(2024, 1, 3, 10, 33, 30),
      userId: authStore.users[2].id,
      historyCheckin: historyStore.historyCheckIn.filter(
        (checkIn) => checkIn.user?.id === authStore.users[2].id
      ),
      historyCheckout: historyStore.historyCheckOut.filter(
        (checkOut) => checkOut.user?.id === authStore.users[2].id
      )
    }
  ])

  return {
    salarys,
    defaultsalary,
    Paystatus
  }
})
