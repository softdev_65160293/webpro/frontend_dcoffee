import { defineStore } from 'pinia'
import type { Check } from '@/types/Check'
import type { OrderReceipt } from '@/types/Order'
import jsPDF from 'jspdf'

export const useconvertPDFStore = defineStore('pdf', () => {

  function printCheckPDF(check: Check){
    const doc = new jsPDF()
    const footerY = doc.internal.pageSize.height - 10
    doc.setFontSize(18)
    doc.text(`CheckReceipt Stock No.${check.id}`, 10, 10)
    
    doc.setFontSize(12)
    const formattedDate = new Date(check.created).toLocaleDateString('en-US', {
        weekday: 'long',
        hour12: false,
        hour: 'numeric',
        minute: '2-digit',
        second: '2-digit',
        day: '2-digit', 
        month: '2-digit', 
        year: 'numeric' 
    })
    doc.text(`Date: ${formattedDate}`, 135, 10)
    doc.text('_________________________________________________________________________________', 10, 20)
    doc.setFontSize(14)
    doc.text('Check By', 10, 30)
    doc.setFontSize(12)
    doc.text(`Employee Name: ${check.user?.fullName}`, 10, 40)
    doc.text(`Employee Email: ${check.user?.email}`, 10, 50)
  
    doc.text('_________________________________________________________________________________', 10, 60)
    let startY = 70
    doc.setFontSize(14)
    doc.text("Item Details:", 10, startY)
    startY += 10
  
    check.checkItems!.forEach((item, index) => {
      const yPos = startY + (index * 10)
      doc.text(`- ${item.name}: Balance ${item.unit} unit`, 15, yPos)
    })
  
    const totalPrice = check.checkItems!.reduce((total, item) => total + (item.price * item.unit), 0)
    doc.text('=========================', 10, startY+ (check.checkItems!.length * 10))
    doc.text(`Stock Value: ${totalPrice} Baht`, 10, startY + (check.checkItems!.length * 10) + 10)
    doc.text('=========================', 10, startY+ (check.checkItems!.length * 10) + 20)

    doc.text('(Signature)', 160, footerY - 20)
    doc.setFontSize(12)
    doc.text('Name: ____________________', 140, footerY - 30)
    
    doc.setFontSize(14)
    doc.text("This document is confidential!", 10, footerY)
  
    doc.save(`check_report_${check.id}.pdf`)
  }
  
  function printOrderPDF(order: OrderReceipt) {
    const doc = new jsPDF()
    const footerY = doc.internal.pageSize.height - 10
    doc.setFontSize(18)
    doc.text(`OrderReceipt Stock No.${order.id}`, 10, 10)
    
    doc.setFontSize(12)
    const formattedDate = new Date(order.created).toLocaleDateString('en-US', {
        weekday: 'long',
        hour12: false,
        hour: 'numeric',
        minute: '2-digit',
        second: '2-digit',
        day: '2-digit', 
        month: '2-digit', 
        year: 'numeric' 
    })
    doc.text(`Date: ${formattedDate}`, 135, 10)
    doc.text('_________________________________________________________________________________', 10, 20)
    doc.setFontSize(14)
    doc.text('Order By', 10, 30)
    doc.setFontSize(12)
    doc.text(`Employee Name: ${order.user?.fullName}`, 10, 40)
    doc.text(`Employee Email: ${order.user?.email}`, 10, 50)
  
    doc.text('_________________________________________________________________________________', 10, 60)
    let startY = 70
    doc.setFontSize(14)
    doc.text("Item Details:", 10, startY)
    startY += 10
  
    order.orderItems!.forEach((item, index) => {
      const yPos = startY + (index * 10)
      doc.text(`- ${item.name}: ${item.price} Baht x ${item.qty}`, 15, yPos)
    })
  
    const totalPrice = order.orderItems!.reduce((total, item) => total + (item.price * item.qty), 0)
    doc.text('=========================', 10, startY+ (order.orderItems!.length * 10))
    doc.text(`Balance: ${totalPrice} Baht`, 10, startY + (order.orderItems!.length * 10) + 10)
    doc.text('=========================', 10, startY+ (order.orderItems!.length * 10) + 20)
  
    doc.text('(Signature)', 160, footerY - 20)
    doc.setFontSize(12)
    doc.text('Name: ____________________', 140, footerY - 30)
    
    doc.setFontSize(14)
    doc.text("This document is confidential!", 10, footerY)
  
    doc.save(`order_report_${order.id}.pdf`)
  }
  
  return { printCheckPDF, printOrderPDF }
})
