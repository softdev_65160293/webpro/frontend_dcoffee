import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Bills } from '@/types/Bills'
import { useLoadingStore } from './loading'
import bill_electricService from '@/services/bill_electric'
import bill_waterService from '@/services/bill_water'
import bill_placeService from '@/services/bill_place'

export const useBillsStore = defineStore('bills', () => {
  const loadingStore = useLoadingStore()

  const defalutBills: Bills = {
    type: 'None',
    usedUnit: 0,
    unitToBath: 0,
    totalPrice: 0,
    date: ''
  }

  // const billsElectric = ref<Bills[]>([
  //   {
  //     id: 1,
  //     type: 'Electric',
  //     usedUnit: 3,
  //     unitToBath: 10,
  //     totalPrice: 30,
  //     date: ''
  //   },
  //   {
  //     id: 2,
  //     type: 'Electric',
  //     usedUnit: 4,
  //     unitToBath: 10,
  //     totalPrice: 40,
  //     date: ''
  //   },
  //   {
  //     id: 3,
  //     type: 'Electric',
  //     usedUnit: 5,
  //     unitToBath: 10,
  //     totalPrice: 50,
  //     date: ''
  //   }
  // ])

  // const billsWater = ref<Bills[]>([
  //   {
  //     id: 1,
  //     type: 'Water',
  //     usedUnit: 10,
  //     unitToBath: 10,
  //     totalPrice: 100,
  //     date: ''
  //   },
  //   {
  //     id: 2,
  //     type: 'Water',
  //     usedUnit: 4,
  //     unitToBath: 10,
  //     totalPrice: 40,
  //     date: ''
  //   },
  //   {
  //     id: 3,
  //     type: 'Water',
  //     usedUnit: 5,
  //     unitToBath: 20,
  //     totalPrice: 100,
  //     date: ''
  //   }
  // ])

  // const billsPlace = ref<Bills[]>([
  //   {
  //     id: 1,
  //     type: 'Place',
  //     usedUnit: 0,
  //     unitToBath: 0,
  //     totalPrice: 4000,
  //     date: ''
  //   },
  //   {
  //     id: 2,
  //     type: 'Place',
  //     usedUnit: 0,
  //     unitToBath: 0,
  //     totalPrice: 5000,
  //     date: ''
  //   },
  //   {
  //     id: 3,
  //     type: 'Place',
  //     usedUnit: 0,
  //     unitToBath: 0,
  //     totalPrice: 3000,
  //     date: ''
  //   }
  // ])

  const bills_e = ref<Bills[]>([])
  const editedBill_e = ref<Bills>(JSON.parse(JSON.stringify(defalutBills)))

  async function getBill_e(id: number) {
    loadingStore.doLoad()
    const res = await bill_electricService.getBill_e(id)
    editedBill_e.value = res.data
    loadingStore.finish()
  }

  async function getBills_e() {
    try {
      loadingStore.doLoad()
      const res = await bill_electricService.getBills_e()
      bills_e.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }

  async function saveBill_e() {
    loadingStore.doLoad()
    const bill = editedBill_e.value
    if (!bill.id) {
      // Add new
      const res = await bill_electricService.addBill_e(bill)
    } else {
      // Update
      const res = await bill_electricService.updateBill_e(bill)
    }

    await getBills_e()
    loadingStore.finish()
  }
  async function deleteBill_e() {
    loadingStore.doLoad()
    const bill = editedBill_e.value
    const res = await bill_electricService.delBill_e(bill)

    await getBills_e()
    loadingStore.finish()
  }

  function clearForm_e() {
    editedBill_e.value = JSON.parse(JSON.stringify(bill_electricService))
  }

  //////////////////////////// Bill_Water /////////////////////////////////////////////////

  const bills_w = ref<Bills[]>([])
  const editedBill_w = ref<Bills>(JSON.parse(JSON.stringify(defalutBills)))

  async function getBill_w(id: number) {
    loadingStore.doLoad()
    const res = await bill_waterService.getBill_w(id)
    editedBill_w.value = res.data
    loadingStore.finish()
  }

  async function getBills_w() {
    try {
      loadingStore.doLoad()
      const res = await bill_waterService.getBills_w()
      bills_w.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }

  async function saveBill_w() {
    loadingStore.doLoad()
    const bill = editedBill_w.value
    if (!bill.id) {
      // Add new
      const res = await bill_waterService.addBill_w(bill)
    } else {
      // Update
      const res = await bill_waterService.updateBill_w(bill)
    }

    await getBills_w()
    loadingStore.finish()
  }
  async function deleteBill_w() {
    loadingStore.doLoad()
    const bill = editedBill_w.value
    const res = await bill_waterService.delBill_w(bill)

    await getBills_w()
    loadingStore.finish()
  }

  function clearForm_w() {
    editedBill_w.value = JSON.parse(JSON.stringify(bill_waterService))
  }

  //////////////////////////// Bill_Place /////////////////////////////////////////////////

  const bills_p = ref<Bills[]>([])
  const editedBill_p = ref<Bills>(JSON.parse(JSON.stringify(defalutBills)))

  async function getBill_p(id: number) {
    loadingStore.doLoad()
    const res = await bill_placeService.getBill_p(id)
    editedBill_p.value = res.data
    loadingStore.finish()
  }

  async function getBills_p() {
    try {
      loadingStore.doLoad()
      const res = await bill_placeService.getBills_p()
      bills_p.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }

  async function saveBill_p() {
    loadingStore.doLoad()
    const bill = editedBill_p.value
    if (!bill.id) {
      // Add new
      const res = await bill_placeService.addBill_p(bill)
    } else {
      // Update
      const res = await bill_placeService.updateBill_p(bill)
    }

    await getBills_p()
    loadingStore.finish()
  }
  async function deleteBill_p() {
    loadingStore.doLoad()
    const bill = editedBill_p.value
    const res = await bill_placeService.delBill_p(bill)

    await getBills_p()
    loadingStore.finish()
  }

  function clearForm_p() {
    editedBill_p.value = JSON.parse(JSON.stringify(bill_placeService))
  }

  return {
    // billsWater,
    // billsPlace,
    // billsElectric,
    defalutBills,
    deleteBill_e,
    getBill_e,
    getBills_e,
    saveBill_e,
    clearForm_e,
    bills_e,
    editedBill_e,
    deleteBill_w,
    getBill_w,
    getBills_w,
    saveBill_w,
    clearForm_w,
    bills_w,
    editedBill_w,
    deleteBill_p,
    getBill_p,
    getBills_p,
    saveBill_p,
    clearForm_p,
    bills_p,
    editedBill_p
  }
})
