import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useMessageStore = defineStore('message', () => {
  const snackbar = ref(false)
  const text = ref('')
  function showMessage(msg: string) {
    text.value = msg
    snackbar.value = true
  }

  const errorDialog = ref(false)

  function showErrorLogin() {
    errorDialog.value = true
  }

  function closeErrorLogin() {
    errorDialog.value = false
  }

  return { showMessage, snackbar, text, closeErrorLogin, showErrorLogin, errorDialog }
})
