import type { Material } from '@/types/Material'
import { defineStore } from 'pinia'
import { ref, watch } from 'vue'
import type { OrderReceipt } from '@/types/Order'
import { useAuthStore } from './auth'
import type { OrderItems } from '@/types/OrderItems'
import orderService from '@/services/orderMat'
import materialService from '@/services/material'
import { useMessageStore } from './message'

export const useOrderStore = defineStore('order', () => {
  const authStore = useAuthStore()
  const messageStore = useMessageStore()

  const orderItems = ref<OrderItems[]>([])
  const order = ref<OrderReceipt>()
  const materials = ref<Material[]>([])
  const orderList = ref<OrderReceipt[]>([])

  const orderDialog = ref(false)
  const conDialog = ref(false)

  const currentUser = authStore.getCurrentUser()
  initOrder()

  watch(orderItems, () => {
    calOrder()
  }, { deep: true })

  async function initOrder() {
    order.value = {
      id: 0,
      created: new Date(),
      totalQty: 0,
      totalPrice: 0,
      userId: currentUser!.id!,
      user: currentUser!,
    }
    orderItems.value = []
    getMaterials()
  }

  function addOrderReceipt(material: Material) {
    const index = orderItems.value.findIndex((item) => item.material?.id === material.id)
    if (index >= 0) {
      orderItems.value[index].qty++
    } else {
      const newReceipt: OrderItems = {
        id: -1,
        name: material.name,
        price: material.price,
        qty: 1,
        materialId: material.id!,
        material: material
      }
      orderItems.value.push(newReceipt)
    }
  }

  function calOrder() {
    order.value!.totalPrice = 0
    order.value!.totalQty = 0
    for (let i = 0; i < orderItems.value.length; i++) {
      order.value!.totalPrice += orderItems.value[i].price * orderItems.value[i].qty
      order.value!.totalQty += orderItems.value[i].qty
    }
  }

  function openDialog() {
    if (orderItems.value.length === 0) {
      return
    } else {
      orderDialog.value = true
    }
  }

  async function getMaterials() {
    const res = await materialService.getMaterials()
    materials.value = res.data
  }

  async function getOrders(){
    const res = await orderService.getOrders()
    orderList.value = res.data
  }

  function closeDialog() {
    orderDialog.value = false
  }

  function Confirm() {
    conDialog.value = true
  }

  async function Order() {
    try {
      for (let i = 0; i < orderItems.value.length; i++) {
        const index = materials.value.findIndex(materials => materials.name === orderItems.value[i].name);
        materials.value[index].unit += orderItems.value[i].qty;
  
        await materialService.updateMaterial(materials.value[index]);
      }
      await orderService.addOrder(order.value!, orderItems.value!)
      closeDialogCon()
      closeDialog()
      initOrder()
    } catch (e: any) {
      messageStore.showMessage(e.message)
    }
    await getMaterials()
  }

  function closeDialogCon() {
    conDialog.value = false
  }

  function removeReceiptItem(orderItem: OrderItems) {
    const index = orderItems.value.findIndex((item) => item === orderItem)
    if (index !== -1) {
      orderItems.value.splice(index, 1)
    }
    if (orderItems.value.length === 0) {
      closeDialog()
      initOrder()
    }
  }

  return {
    openDialog,
    calOrder,
    addOrderReceipt,
    closeDialog,
    Order,
    Confirm,
    closeDialogCon,
    removeReceiptItem,
    getMaterials,
    initOrder,
    getOrders,
    materials,
    orderDialog,
    order,
    orderItems,
    conDialog,
    orderList
  }
})
