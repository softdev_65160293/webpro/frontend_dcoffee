import { ref } from 'vue'
import { defineStore } from 'pinia'
import type_ProductService from '@/services/type_product'
import type { ProductType } from '@/types/ProductType'

export const useTypeProductStore = defineStore('typeproduct', () => {
  const types = ref<ProductType[]>([])
  const initialType: ProductType = {
    name: ''
  }
  const editedType = ref<ProductType>(JSON.parse(JSON.stringify(initialType)))

  async function getType(id: number) {
    const res = await type_ProductService.getType_P(id)
    editedType.value = res.data
  }
  async function getTypes() {
    const res = await type_ProductService.getTypes_P()
    types.value = res.data
    console.log(types.value)
  }

  async function saveType() {
    const type = editedType.value
    if (!type.id) {
      // Add new
      console.log('Post ' + JSON.stringify(type))
      const res = await type_ProductService.addType_P(type)
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(type))
      const res = await type_ProductService.updateType_P(type)
    }
    await getTypes()
  }
  async function deleteType() {
    const type = editedType.value
    const res = await type_ProductService.delType_P(type)

    await getTypes()
  }

  function clearForm() {
    editedType.value = JSON.parse(JSON.stringify(initialType))
  }
  return { types, getTypes, saveType, deleteType, editedType, getType, clearForm }
})
