import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useAuthStore } from './auth'
import type { User } from '@/types/User'
import type { History } from '@/types/History'

export const useHistoryStore = defineStore('history', () => {
  const authStore = useAuthStore()

  const defaultHistory = ref<History>({
    id: -1,
    timeDate: new Date(),
    user: authStore.currentUser!,
    late: false
  })

  const historyCheckIn = ref<History[]>([
    {
      id: 1,
      timeDate: new Date(2024, 1, 1, 9, 43, 30),
      user: authStore.users[0],
      late: false
    },
    {
      id: 2,
      timeDate: new Date(2024, 1, 2, 8, 59, 30),
      user: authStore.users[1],
      late: true
    },
    {
      id: 3,
      timeDate: new Date(2024, 1, 3, 10, 33, 30),
      user: authStore.users[2],
      late: false
    },
    {
      id: 4,
      timeDate: new Date(2024, 1, 1, 9, 43, 30),
      user: authStore.users[0],
      late: false
    }
  ])

  const historyCheckOut = ref<History[]>([
    {
      id: 1,
      timeDate: new Date(2024, 1, 1, 11, 43, 30),
      user: authStore.users[0],
      late: true
    },
    {
      id: 2,
      timeDate: new Date(2024, 1, 2, 12, 53, 30),
      user: authStore.users[1],
      late: false
    },
    {
      id: 3,
      timeDate: new Date(2024, 1, 3, 13, 33, 30),
      user: authStore.users[2],
      late: true
    },
    {
      id: 4,
      timeDate: new Date(2024, 1, 3, 13, 33, 30),
      user: authStore.users[0],
      late: true
    }
  ])

  const status = ref(false)

  const checkCorrectIdPass = (username: string, password: string) => {
    const indexUsername = authStore.users.findIndex((item) => item.username === username)
    status.value = false
    if (indexUsername < 0) {
      return (status.value = false)
    } else if (
      password === authStore.users[indexUsername].password &&
      username === authStore.currentUser?.username &&
      password === authStore.currentUser?.password
    ) {
      return (status.value = true)
    }
  }

  return { defaultHistory, historyCheckIn, historyCheckOut, checkCorrectIdPass, status }
})
