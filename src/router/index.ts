import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('../views/HomeView.vue'),
      meta: {
        layout: 'Unlock_in',
        requireAuth: false
      }
    },
    {
      path: '/MainMenu',
      name: 'mainmenu',
      component: () => import('../views/MainMenuView.vue'),
      meta: {
        layout: 'Lock_in',
        requireAuth: true
      }
    },
    {
      path: '/Product',
      name: 'product',
      component: () => import('../views/Product/ProductView.vue'),
      meta: {
        layout: 'Lock_in',
        requireAuth: true
      }
    },
    {
      path: '/Employee',
      name: 'employee',
      component: () => import('../views/EmployeeView.vue'),
      meta: {
        layout: 'Lock_in',
        requireAuth: true
      }
    },
    {
      path: '/Member',
      name: 'member',
      component: () => import('../views/Member/MemberView.vue'),
      meta: {
        layout: 'Lock_in',
        requireAuth: true
      }
    },
    {
      path: '/POS',
      name: 'pos',
      component: () => import('../views/POS/POSView.vue'),
      meta: {
        layout: 'Lock_in',
        requireAuth: true
      }
    },
    {
      path: '/Stock',
      name: 'stock',
      component: () => import('../views/Stock/StockView.vue'),
      meta: {
        layout: 'Lock_in',
        requireAuth: true
      }
    },
    {
      path: '/Bills',
      name: 'bills',
      component: () => import('../views/BillsView.vue'),
      meta: {
        layout: 'Lock_in',
        requireAuth: true
      }
    },
    {
      path: '/CheckIn',
      name: 'checkin',
      component: () => import('../views/CheckInOutView.vue'),
      meta: {
        layout: 'Lock_in',
        requireAuth: true
      }
    },
    {
      path: '/Salary',
      name: 'salary',
      component: () => import('../views/SalaryView.vue'),
      meta: {
        layout: 'Lock_in',
        requireAuth: true
      }
    },
    {
      path: '/Receipt',
      name: 'receipt',
      component: () => import('../views/ReceiptView.vue'),
      meta: {
        layout: 'Lock_in',
        requireAuth: true
      }
    },
    {
      path: '/CheckStock',
      name: 'checkstock',
      component: () => import('../views/Stock/CheckStockView.vue'),
      meta: {
        layout: 'Lock_in',
        requireAuth: true
      }
    },
    {
      path: '/OrderStock',
      name: 'orderstock',
      component: () => import('../views/Stock/OrderStockView.vue'),
      meta: {
        layout: 'Lock_in',
        requireAuth: true
      }
    },
    {
      path: '/History',
      name: 'history',
      component: () => import('../views/Stock/HistoryStockView.vue'),
      meta: {
        layout: 'Lock_in',
        requireAuth: true
      }
    },
    {
      path: '/MainMenuMember',
      name: 'mainmenumember',
      component: () => import('../views/Member_Login_View/MainMenuMemberView.vue'),
      meta: {
        layout: 'Lock_in_Member',
        requireAuth: true
      }
    },
    {
      path: '/Purchase',
      name: 'purchase',
      component: () => import('../views/Member_Login_View/PurchaseMemberView.vue'),
      meta: {
        layout: 'Lock_in_Member',
        requireAuth: true
      }
    },
    {
      path: '/PromotionMember',
      name: 'promotionmember',
      component: () => import('../views/Member_Login_View/PromotionMemberView.vue'),
      meta: {
        layout: 'Lock_in_Member',
        requireAuth: true
      }
    },
    {
      path: '/HistoryMember',
      name: 'Historymember',
      component: () => import('../views/Member_Login_View/HistoryMemberView.vue'),
      meta: {
        layout: 'Lock_in_Member',
        requireAuth: true
      }
    },
    {
      path: '/AccountMember',
      name: 'accountmember',
      component: () => import('../views/Member_Login_View/AccountMemberView.vue'),
      meta: {
        layout: 'Lock_in_Member',
        requireAuth: true
      }
    }
  ]
})

function isLogin() {
  const user = localStorage.getItem('user')
  if (user) {
    return true
  } else {
    return false
  }
}

router.beforeEach((to, from) => {
  console.log(to)
  console.log(from)
  if (to.meta.requireAuth && !isLogin()) {
    return {
      path: '/',
      query: { redircet: to.fullPath }
    }
  }
})

export default router
