import http from './http'
import type { ReceiptItem } from '@/types/ReceiptItem';
import type { Receipt } from '@/types/Receipt';

type ReceiptDto = {
  orderItems: {
    productId: number;
    qty: number;
  }[];
  userId: number;
  memberId: number;
  paymentType: string;
  discount: number;
  receicedAmount: number;
}

function addReceipt(receipt: Receipt, receiptItems: ReceiptItem[]) {
  const receiptDto: ReceiptDto = {
    orderItems: [],
    userId: 0,
    memberId: 0,
    paymentType: '',
    discount: 0,
    receicedAmount: 0,
  }
  receiptDto.userId = receipt.userId!
  receiptDto.memberId = receipt.memberID!
  console.log(receipt.memberID)
  receiptDto.orderItems = receiptItems.map((item) => {
    return {
      productId: item.productId,
      qty: item.unit
    }
  })
  receiptDto.paymentType = receipt.paymentType
  receiptDto.discount = receipt.memberDiscount
  receiptDto.receicedAmount = receipt.receicedAmount

  return http.post('/receipts', receiptDto)
}

async function getReceipt() {
  return await http.get('/receipts')
}

export default { addReceipt, getReceipt }
