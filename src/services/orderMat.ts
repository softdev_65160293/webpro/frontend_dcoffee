import http from './http'
import type { OrderItems } from '@/types/OrderItems';
import type { OrderReceipt } from '@/types/Order';

type OrderDto = {
  orderItems: {
    materialId: number;
    qty: number;
  }[]
  userId: number;
}

async function addOrder(order: OrderReceipt, orderItems: OrderItems[]) {
  try {
    const orderDto: OrderDto = {
      orderItems: [],
      userId: 0
    }
    orderDto.userId = order.userId
    orderDto.orderItems = orderItems.map((item) => {
      return {
        materialId: item.materialId,
        qty: item.qty
      }
    })

    console.log('Sending check DTO:', orderDto);
    await http.post('/orders', orderDto);
    console.log('Check successfully added.');
  } catch (error) {
    console.error('Error adding check:', error);
    throw error;
  }
}

async function getOrders() {
  return await http.get('/orders')
}
export default { addOrder, getOrders }
