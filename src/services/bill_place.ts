import http from './http'
import type { Bills } from '@/types/Bills'

function addBill_p(bill: Bills) {
  return http.post('/bills_place', bill)
}

function updateBill_p(bill: Bills) {
  return http.patch(`/bills_place/${bill.id}`, bill)
}

function delBill_p(bill: Bills) {
  return http.delete(`/bills_place/${bill.id}`)
}

function getBill_p(id: number) {
  return http.get(`/bills_place/${id}`)
}

function getBills_p() {
  return http.get('/bills_place')
}

export default { addBill_p, updateBill_p, delBill_p, getBill_p, getBills_p }
