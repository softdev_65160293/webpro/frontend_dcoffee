import http from './http'
import type { ProductType } from '@/types/ProductType'

function addType_P(type: ProductType) {
  return http.post('/types_product', type)
}

function updateType_P(type: ProductType) {
  return http.patch(`/types_product/${type.id}`, type)
}

function delType_P(type: ProductType) {
  return http.delete(`/types_product/${type.id}`)
}

function getType_P(id: number) {
  return http.get(`/types_product/${id}`)
}

function getTypes_P() {
  return http.get('/types_product')
}

export default { addType_P, updateType_P, delType_P, getType_P, getTypes_P }
