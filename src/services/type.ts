import type { MaterialType } from '@/types/MaterialType'
import http from './http'

function addType(type: MaterialType) {
  return http.post('/types', type)
}

function updateType(type: MaterialType) {
  return http.patch(`/types/${type.id}`, type)
}

function delType(type: MaterialType) {
  return http.delete(`/types/${type.id}`)
}

function getType(id: number) {
  return http.get(`/types/${id}`)
}

function getTypes() {
  return http.get('/types')
}

export default { addType, updateType, delType, getType, getTypes }
