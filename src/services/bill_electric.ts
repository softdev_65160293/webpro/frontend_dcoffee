import http from './http'
import type { Bills } from '@/types/Bills'

function addBill_e(bill: Bills) {
  return http.post('/bills_electric', bill)
}

function updateBill_e(bill: Bills) {
  return http.patch(`/bills_electric/${bill.id}`, bill)
}

function delBill_e(bill: Bills) {
  return http.delete(`/bills_electric/${bill.id}`)
}

function getBill_e(id: number) {
  return http.get(`/bills_electric/${id}`)
}

function getBills_e() {
  return http.get('/bills_electric')
}

export default { addBill_e, updateBill_e, delBill_e, getBill_e, getBills_e }
