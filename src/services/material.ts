import type { Material } from '@/types/Material'
import http from './http'

function addMaterial(material: Material) {
  return http.post('/materials/', material) //add
}

function updateMaterial(material: Material) {
  return http.patch('/materials/' + material.id, material) //update
}

function removeMaterial(material: Material) {
  return http.delete('/materials/' + material.id) //delete
}

function getMaterial(id: number) {
  return http.get('/materials/' + id)
}

function getMaterialsByType(typeId: number) {
    return http.get('/material/type/'+ typeId)
  }

function getMaterials() {
  return http.get('/materials')
}

export default { addMaterial, updateMaterial, removeMaterial, getMaterial, getMaterials, getMaterialsByType }
