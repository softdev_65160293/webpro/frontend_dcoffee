import http from './http'
import type { Bills } from '@/types/Bills'

function addBill_w(bill: Bills) {
  return http.post('/bills_water', bill)
}

function updateBill_w(bill: Bills) {
  return http.patch(`/bills_water/${bill.id}`, bill)
}

function delBill_w(bill: Bills) {
  return http.delete(`/bills_water/${bill.id}`)
}

function getBill_w(id: number) {
  return http.get(`/bills_water/${id}`)
}

function getBills_w() {
  return http.get('/bills_water')
}

export default { addBill_w, updateBill_w, delBill_w, getBill_w, getBills_w }
