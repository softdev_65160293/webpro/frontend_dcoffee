import type { Member } from '@/types/Member'
import http from './http'

function addMember(member: Member & { files: File[] }) {
  const formData = new FormData()
  formData.append('email', member.email)
  formData.append('username', member.username)
  formData.append('name', member.name)
  formData.append('tel', member.tel)
  formData.append('password', member.password)
  formData.append('point', member.point.toString())
  formData.append('gender', member.gender)
  if (member.files && member.files.length > 0) {
    formData.append('file', member.files[0])
  }
  return http.post('/members/', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updateMember(member: Member & { files: File[] }) {
  const formData = new FormData()
  formData.append('email', member.email)
  formData.append('username', member.username)
  formData.append('name', member.name)
  formData.append('tel', member.tel)
  formData.append('password', member.password)
  formData.append('point', member.point.toString())
  formData.append('gender', member.gender)
  if (member.files && member.files.length > 0) {
    formData.append('file', member.files[0])
  }
  return http.post(`/members/${member.id}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function removeMember(member: Member) {
  return http.delete(`/members/${member.id}`) //delete
}

function getMember(id: number) {
  return http.get(`/members/${id}`)
}

function getMemberByTel(tel: string) {
  return http.get(`/members/tel/${tel}`)
}

function getMembers() {
  return http.get('/members')
}

export default { addMember, updateMember, removeMember, getMember, getMembers, getMemberByTel }
