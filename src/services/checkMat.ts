import http from './http'
import type { Check } from '@/types/Check';
import type { CheckItems } from '@/types/CheckItems';

type CheckDto = {
  checkItems: {
    materialId: number;
    qty: number;
  }[]
  userId: number;
}

async function addCheck(check: Check, checkItems: CheckItems[]) {
  try {
    const checkDto: CheckDto = {
      checkItems: [],
      userId: check.userId
    };

    checkDto.checkItems = checkItems.map(item => ({
      materialId: item.materialId,
      qty: item.unit
    }));

    console.log('Sending check DTO:', checkDto);
    await http.post('/check', checkDto);
    console.log('Check successfully added.');
  } catch (error) {
    console.error('Error adding check:', error);
    throw error;
  }
}

async function getChecks(){
  return await http.get('/check')
}

export default { addCheck, getChecks }
