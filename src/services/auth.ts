import http from './http'

function login(username: string, password: string) {
  return http.post('/auth/login', { username, password })
}

export default { login }
